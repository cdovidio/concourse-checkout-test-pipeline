#!/bin/bash
apt-get update && apt-get upgrade -y
#get git lfs install script
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
curl -sL https://deb.nodesource.com/setup_10.x | bash
#add repo for jdk then install nodejs, npm, jdk8, ruby, essential build tools and git lfs
apt-get install -y nodejs npm ruby-full build-essential git-lfs
#startup git lfs (although it should already be started automatically when installed)
git lfs install
#install gulp
npm install --global gulp
gem install bundler --no-ri --no-rdoc
cd resource-uc2
JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64 bundle install --jobs $(nproc) "${FLAGS[@]}"